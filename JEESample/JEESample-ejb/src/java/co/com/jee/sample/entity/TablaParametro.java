/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.jee.sample.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ferne
 */
@Entity
@Table(name = "TABLA_PARAMETRO", catalog = "", schema = "ANGULAR")
@NamedQueries({
    @NamedQuery(name = "TablaParametro.findAll", query = "SELECT t FROM TablaParametro t")
    , @NamedQuery(name = "TablaParametro.findById", query = "SELECT t FROM TablaParametro t WHERE t.id = :id")
    , @NamedQuery(name = "TablaParametro.findByNombre", query = "SELECT t FROM TablaParametro t WHERE t.nombre = :nombre")})
public class TablaParametro implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 19, scale = 0)
    private BigDecimal id;
    @Size(max = 255)
    @Column(length = 255)
    private String nombre;
    @OneToMany(mappedBy = "tablaParametroId")
    private List<CampoParametro> campoParametroList;

    public TablaParametro() {
    }

    public TablaParametro(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<CampoParametro> getCampoParametroList() {
        return campoParametroList;
    }

    public void setCampoParametroList(List<CampoParametro> campoParametroList) {
        this.campoParametroList = campoParametroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TablaParametro)) {
            return false;
        }
        TablaParametro other = (TablaParametro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.jee.sample.entity.TablaParametro[ id=" + id + " ]";
    }
    
}
