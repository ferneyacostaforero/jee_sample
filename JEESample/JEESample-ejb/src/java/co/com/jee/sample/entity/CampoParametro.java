/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.jee.sample.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ferne
 */
@Entity
@Table(name = "CAMPO_PARAMETRO", catalog = "", schema = "ANGULAR")
@NamedQueries({
    @NamedQuery(name = "CampoParametro.findAll", query = "SELECT c FROM CampoParametro c")
    , @NamedQuery(name = "CampoParametro.findById", query = "SELECT c FROM CampoParametro c WHERE c.id = :id")
    , @NamedQuery(name = "CampoParametro.findByCodigo", query = "SELECT c FROM CampoParametro c WHERE c.codigo = :codigo")
    , @NamedQuery(name = "CampoParametro.findByNombre", query = "SELECT c FROM CampoParametro c WHERE c.nombre = :nombre")
    , @NamedQuery(name = "CampoParametro.findByValor", query = "SELECT c FROM CampoParametro c WHERE c.valor = :valor")})
public class CampoParametro implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 19, scale = 0)
    private BigDecimal id;
    @Size(max = 255)
    @Column(length = 255)
    private String codigo;
    @Size(max = 255)
    @Column(length = 255)
    private String nombre;
    @Size(max = 255)
    @Column(length = 255)
    private String valor;
    @JoinColumn(name = "TABLA_PARAMETRO_ID", referencedColumnName = "ID")
    @ManyToOne
    private TablaParametro tablaParametroId;

    public CampoParametro() {
    }

    public CampoParametro(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TablaParametro getTablaParametroId() {
        return tablaParametroId;
    }

    public void setTablaParametroId(TablaParametro tablaParametroId) {
        this.tablaParametroId = tablaParametroId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CampoParametro)) {
            return false;
        }
        CampoParametro other = (CampoParametro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.jee.sample.entity.CampoParametro[ id=" + id + " ]";
    }
    
}
