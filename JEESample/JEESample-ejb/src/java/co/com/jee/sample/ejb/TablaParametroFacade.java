/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.jee.sample.ejb;

import co.com.jee.sample.entity.TablaParametro;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Ferne
 */
@Stateless
@LocalBean
public class TablaParametroFacade extends AbstractFacade<TablaParametro> {

    @PersistenceContext(unitName = "JEESample-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TablaParametroFacade() {
        super(TablaParametro.class);
    }

}
