/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.jee.sample.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ferne
 */
@Entity
@Table(catalog = "", schema = "ANGULAR")
@NamedQueries({
    @NamedQuery(name = "Auditoria.findAll", query = "SELECT a FROM Auditoria a")
    , @NamedQuery(name = "Auditoria.findById", query = "SELECT a FROM Auditoria a WHERE a.id = :id")
    , @NamedQuery(name = "Auditoria.findByController", query = "SELECT a FROM Auditoria a WHERE a.controller = :controller")
    , @NamedQuery(name = "Auditoria.findByDateAudit", query = "SELECT a FROM Auditoria a WHERE a.dateAudit = :dateAudit")
    , @NamedQuery(name = "Auditoria.findByTypeRequest", query = "SELECT a FROM Auditoria a WHERE a.typeRequest = :typeRequest")
    , @NamedQuery(name = "Auditoria.findByUserRequest", query = "SELECT a FROM Auditoria a WHERE a.userRequest = :userRequest")})
public class Auditoria implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 19, scale = 0)
    private BigDecimal id;
    @Size(max = 255)
    @Column(length = 255)
    private String controller;
    @Column(name = "DATE_AUDIT")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAudit;
    @Lob
    @Column(name = "JSON_STR")
    private String jsonStr;
    @Size(max = 255)
    @Column(name = "TYPE_REQUEST", length = 255)
    private String typeRequest;
    @Size(max = 255)
    @Column(name = "USER_REQUEST", length = 255)
    private String userRequest;

    public Auditoria() {
    }

    public Auditoria(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getController() {
        return controller;
    }

    public void setController(String controller) {
        this.controller = controller;
    }

    public Date getDateAudit() {
        return dateAudit;
    }

    public void setDateAudit(Date dateAudit) {
        this.dateAudit = dateAudit;
    }

    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }

    public String getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(String typeRequest) {
        this.typeRequest = typeRequest;
    }

    public String getUserRequest() {
        return userRequest;
    }

    public void setUserRequest(String userRequest) {
        this.userRequest = userRequest;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Auditoria)) {
            return false;
        }
        Auditoria other = (Auditoria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.jee.sample.entity.Auditoria[ id=" + id + " ]";
    }
    
}
