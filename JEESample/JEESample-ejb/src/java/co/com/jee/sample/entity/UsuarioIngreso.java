/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.jee.sample.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Ferne
 */
@Entity
@Table(name = "USUARIO_INGRESO", catalog = "", schema = "ANGULAR")
@NamedQueries({
    @NamedQuery(name = "UsuarioIngreso.findAll", query = "SELECT u FROM UsuarioIngreso u")
    , @NamedQuery(name = "UsuarioIngreso.findById", query = "SELECT u FROM UsuarioIngreso u WHERE u.id = :id")
    , @NamedQuery(name = "UsuarioIngreso.findByCreatedDate", query = "SELECT u FROM UsuarioIngreso u WHERE u.createdDate = :createdDate")
    , @NamedQuery(name = "UsuarioIngreso.findByExpireIn", query = "SELECT u FROM UsuarioIngreso u WHERE u.expireIn = :expireIn")
    , @NamedQuery(name = "UsuarioIngreso.findByIp", query = "SELECT u FROM UsuarioIngreso u WHERE u.ip = :ip")
    , @NamedQuery(name = "UsuarioIngreso.findByToken", query = "SELECT u FROM UsuarioIngreso u WHERE u.token = :token")})
public class UsuarioIngreso implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(nullable = false, precision = 19, scale = 0)
    private BigDecimal id;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "EXPIRE_IN")
    private Long expireIn;
    @Size(max = 255)
    @Column(length = 255)
    private String ip;
    @Size(max = 255)
    @Column(length = 255)
    private String token;
    @JoinColumn(name = "USUARIO", referencedColumnName = "ID", nullable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public UsuarioIngreso() {
    }

    public UsuarioIngreso(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Long getExpireIn() {
        return expireIn;
    }

    public void setExpireIn(Long expireIn) {
        this.expireIn = expireIn;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UsuarioIngreso)) {
            return false;
        }
        UsuarioIngreso other = (UsuarioIngreso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.jee.sample.entity.UsuarioIngreso[ id=" + id + " ]";
    }
    
}
